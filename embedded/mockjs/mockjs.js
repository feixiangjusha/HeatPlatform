import Mock from 'mockjs'
import qs from 'qs'
import response from './response'
function param2Obj(url) {
    const search = url.split('?')[1]
    if (!search) {
        return {}
    }
    return JSON.parse(
        '{"' +
        decodeURIComponent(search)
            .replace(/"/g, '\\"')
            .replace(/&/g, '","')
            .replace(/=/g, '":"')
            .replace(/\+/g, ' ') +
        '"}'
    )
}

// for front mock
// please use it cautiously, it will redefine XMLHttpRequest,
// which will cause many of your third-party libraries to be invalidated(like progress event).
export function mockXHR() {
    let mocks = response


    Mock.XHR.prototype.proxy_send = Mock.XHR.prototype.send
    Mock.XHR.prototype.send = function () {
        if (this.custom.xhr) {
            this.custom.xhr.withCredentials = this.withCredentials || false

            if (this.responseType) {
                this.custom.xhr.responseType = this.responseType
            }
        }
        this.proxy_send(...arguments)
    }

    function XHR2ExpressReqWrap(respond) {
        return function (options) {
            let result = null
            if (respond instanceof Function) {
                const { body, type, url } = options
                // https://expressjs.com/en/4x/api.html#req
                let objBody = {}
                try {
                    objBody = JSON.parse(body)
                } catch (e) {
                    try {
                        objBody = qs.parse(body)
                    } catch (e) {
                    }
                }
                result = respond({
                    method: type,
                    body: objBody,
                    query: param2Obj(url)
                })
            } else {
                result = respond
            }
            return Mock.mock(result)
        }
    }
    for (const i of mocks) {
        if (i.enable) {
            Mock.mock(new RegExp('^(' + i.url + '){1}(\\?.*)?$'), i.type || 'get', XHR2ExpressReqWrap(i.response))
        }
    }
}
