const basePath = ""
import Mock from 'mockjs'

let heating = false;
let targetTemp = 50;
let PID = {P: 0, I: 0, D: 0};
export default [
    {
        enable: true,
        url: basePath + '/state',
        type: 'get',
        response: config => {
            if (config.query != undefined && config.query.heating != undefined) {
                heating = config.query.heating
            }
            return {
                success: true,
                heating: heating,
                ["targetTemp|" + targetTemp]: 1,
                currTemp: heating ? ('@integer(' + (targetTemp - 50) + ', ' + targetTemp + ')') : '@integer(25, 50)',
                "voltage|20": 1,
                current: heating ? (Mock.Random.float(30, 32, 0, 1) / 10).toFixed(2) : 0
            }

        }
    }, {
        enable: true,
        url: basePath + '/setTemp',
        type: 'get',
        response: config => {
            if (config.query != undefined && config.query.temp != undefined) {
                targetTemp = config.query.temp
            }
            return {
                success: true,
                ["targetTemp|" + targetTemp]: 1,
            }

        }
    },
    {
        enable: true,
        url: basePath + '/PID',
        type: 'get',
        response: config => {

            return {
                success: true,
                ...PID,
            }

        }
    },
    {
        enable: true,
        url: basePath + '/PID',
        type: 'post',
        response: config => {
            PID = config.body
            return {
                success: true,
                ...PID,
            }

        }
    },
]
