import axios from 'axios'


// 创建 axios 实例
const request = axios.create({
    baseURL: "/", // api base_url
    timeout: 5000 // 请求超时时间
})

const err = (error) => {
    return Promise.reject(error)
};

// request interceptor
request.interceptors.request.use(config => {
    return config
}, (error) => {
    return Promise.reject(error)
})

// response interceptor
request.interceptors.response.use((response) => {
    return response.data
}, err)


//post
export function postAction(url, parameter) {
    return request({
        url: url,
        method: 'post',
        data: parameter
    })
}

//post method= {post | put}
export function httpAction(url, parameter, method) {
    return request({
        url: url,
        method: method,
        data: parameter
    })
}

//put
export function putAction(url, parameter) {
    return request({
        url: url,
        method: 'put',
        data: parameter
    })
}

//get
export function getAction(url, parameter) {
    return request({
        url: url,
        method: 'get',
        params: parameter
    })
}
