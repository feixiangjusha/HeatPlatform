import Vue from 'vue'
import VueRouter from 'vue-router'
import {RouterMap} from "@/config/router.config";
import Layout from "@/components/Layout";

Vue.use(VueRouter)

RouterMap.push({
    path: '*', redirect: RouterMap[0].path, hidden: true
})

const router = new VueRouter({
    routes: RouterMap
})

export default router
