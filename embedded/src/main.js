import Vue from 'vue'
import App from './App.vue'
import router from './router'

if (process.env.NODE_ENV == "development") {
    console.log("enable mock");
    const {mockXHR} = require("../mockjs/mockjs")
    mockXHR();
}
Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
