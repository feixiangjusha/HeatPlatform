/**
 * 路由,一级扁平,不支持嵌套
 * @type { *[] }
 */
export const RouterMap = [
    //第一个会作为默认页,当访问不存在的页面时也会跳到这里
    {
        path: '/',
        title: '首页',
        icon: 'home-o',
        component: require(`@views/HomeView`).default,

    },
    {
        path: '/config',
        title: '设置',
        icon: 'setting-o',
        component: require(`@views/ConfigView`).default,
    }
]
