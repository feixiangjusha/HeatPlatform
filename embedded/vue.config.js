const {defineConfig} = require('@vue/cli-service')
const path = require('path')

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlMinimizerPlugin = require("html-minimizer-webpack-plugin");
const HtmlInlineScriptPlugin = require("html-inline-script-webpack-plugin");
const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;
const Compression = require("compression-webpack-plugin");

function resolve(dir) {
    return path.join(__dirname, dir)
}


// vue.config.js
module.exports = defineConfig({
        runtimeCompiler: true,

        // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
        productionSourceMap: false,
        //打包app时放开该配置
        publicPath: '/',
        configureWebpack: config => {
            //生产环境取消 console.log
            if (process.env.NODE_ENV === 'production') {
                config.plugins.push(
                    new CleanWebpackPlugin(),
                    new MiniCssExtractPlugin({
                        filename: "[name].css",
                        chunkFilename: "[id].css",
                    }),
                    // //script内嵌无法嵌套路由js，所以路由需要通过非懒加载方式导入
                    new HtmlInlineScriptPlugin(
                        {
                            scriptMatchPattern: [/\.(js)$/],
                        }
                    ),
                    new HTMLInlineCSSWebpackPlugin(),
                    new Compression({
                        test: /\.(html)$/,
                        filename: "[path]../dist/[base].gz",
                        algorithm: "gzip",
                        exclude: /.map$/,
                        deleteOriginalAssets: "keep-source-map",
                    }),
                )
            }
        },
        chainWebpack: (config) => {
            config.resolve.alias
                .set('@$', resolve('src'))
                .set('@api', resolve('src/api'))
                .set('@assets', resolve('src/assets'))
                .set('@comp', resolve('src/components'))
                .set('@views', resolve('src/views'))

            config
                .plugin('html')
                .use(new HtmlWebpackPlugin({
                    filename: "index.html",
                    templateParameters: {
                        BASE_URL: `/`
                    },
                    template: resolve("./public/index.html"),
                    inlineSource: ".(js|css)$",
                    inject: "body",
                }))

        },
        css: {
            // loaderOptions: {
            //     less: {
            //         javascriptEnabled: true,
            //     }
            // },
        },

        devServer: {
            port: 6699,
            proxy: {
                '/update': {
                    target: 'http://localhost:6699/update.html',
                    pathRewrite: {
                        '/update': ''
                    }
                },
            }
        },

        // lintOnSave: false
    }
)
