import os
import subprocess
import shutil

os.chdir("./embedded")

npm_path = shutil.which('npm')  # 查找npm可执行文件的路径
if npm_path is not None:
    try:
        # 指定npm的绝对路径并执行命令
        subprocess.check_output([npm_path, '--version'])
        #打包前端页面
        os.system('npm run build')
    except FileNotFoundError:
        print("npm 未安装")
    except subprocess.CalledProcessError:
        print("npm 未安装")
else:
    print("npm 未安装")




