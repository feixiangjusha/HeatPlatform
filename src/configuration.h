#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H
#include "./base/espdefines.h"

/**
 * @brief 调试日志输出路径
 * DEBUG_OUTPUT_NO
 * DEBUG_OUTPUT_SERIAL0
 * DEBUG_OUTPUT_SERIAL1
 * DEBUG_OUTPUT_SERIAL2
 * DEBUG_OUTPUT_WEBSOCKET
 */
#define DEBUG_OUTPUT_CHANNEL DEBUG_OUTPUT_WEBSOCKET
/**
 * @brief 日志输出级别
 * DEBUG_LEVEL_DEBUG
 * DEBUG_LEVEL_INFO
 * DEBUG_LEVEL_ERROR
 * 由上到下级别增加，低于设定级别的日志不输出
 */
#define DEBUG_LEVEL DEBUG_LEVEL_DEBUG
/**
 * @brief 调试日志串口输出波特率
 *
 */
#define DEBUG_BAUDRATE 115200
/**
 * @brief 调试日志websocket输出端口
 *
 */
#define DEBUG_OUTPUT_PORT 8000

/**
 * @brief 默认WIFI模式
 * ESP_WIFI_STA
 * ESP_WIFI_AP
 * 如果配置了STA模式且STA模式连接失败会转为AP模式
 */
#define DEFAULT_WIFI_MODE ESP_WIFI_AP

#define DEFAULT_AP_SSID "HeatPlatform"
#define DEFAULT_AP_PASSWORD "12345678"

#define DEFAULT_STA_SSID "*********"
#define DEFAULT_STA_PASSWORD "*******"
#define DEFAULT_STA_IP "192.168.0.1"
// eeporm保存数字有问题,先用string格式
#define DEFAULT_HEATING_TEMPERATURE "50"

#define DEFAULT_PID_P "70"

#define DEFAULT_PID_I "0.05"

#define DEFAULT_PID_D "0"
/* Settings location
 * SETTINGS_IN_EEPROM //ESP8266/ESP32
 * SETTINGS_IN_PREFERENCES //ESP32 only
 *  Location where ESP3D will save settings
 */
#define ESP_SAVE_SETTINGS SETTINGS_IN_EEPROM

/**
 * @brief 配置持久化版本号，需要重新覆盖默认项时修改为不同值
 * 长度1个字符
 */
#define SETTINGS_VERSION "1"
#endif //_CONFIGURATION_H
