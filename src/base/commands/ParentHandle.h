#ifndef _PARENTHANDLE_H
#define _PARENTHANDLE_H
#include "../espconfig.h"
#include "map"
class ParentHandle
{


public:
    ParentHandle();
    String getCMD();
    bool handle(const char *cmd_params);
};

#endif