#ifndef _ESP_CLOCK_H
#define _ESP_CLOCK_H
void clockInit();
unsigned long getMillis(void);
#endif