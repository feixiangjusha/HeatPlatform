
#include "espconfig.h"
#include "espoutput.h"

#include "espsettings.h"
// #if defined (HTTP_FEATURE) || defined(WS_DATA_FEATURE)
// #include "../modules/websocket/websocket_server.h"
// #endif //HTTP_FEATURE || WS_DATA_FEATURE

#if defined(ARDUINO_ARCH_ESP32)
#include <WebServer.h>
#endif // ARDUINO_ARCH_ESP32
#if defined(ARDUINO_ARCH_ESP8266)
#include <ESP8266WebServer.h>
#endif // ARDUINO_ARCH_ESP8266

// tool function to avoid string corrupt JSON files
const char *ESPOutput::encodeString(const char *s)
{
    static String tmp;
    tmp = s;
    while (tmp.indexOf("'") != -1)
    {
        tmp.replace("'", "&#39;");
    }
    while (tmp.indexOf("\"") != -1)
    {
        tmp.replace("\"", "&#34;");
    }
    if (tmp == "")
    {
        tmp = " ";
    }
    return tmp.c_str();
}

// constructor
ESPOutput::ESPOutput(WEBSERVER *webserver)
{
    _code = 200;
    _headerSent = false;
    _footerSent = false;
    _webserver = webserver;
}

// destructor
ESPOutput::~ESPOutput()
{
    flush();
}

size_t ESPOutput::dispatch(const uint8_t *sbuf, size_t len)
{
    log_debug("Dispatch %d chars", len);

    log_debug("Dispatch websocket terminal");
    // websocket_terminal_server.write(sbuf, len);

    return len;
}

// Flush
void ESPOutput::flush()
{
    if (_webserver)
    {
        if (_headerSent && !_footerSent)
        {
            _webserver->sendContent("");
            _footerSent = true;
        }
    }
}

size_t ESPOutput::printLN(const char *s)
{

    if (strlen(s) > 0)
    {
        println(s);
        return strlen(s) + 1;
    }
    else
    {
        println(" ");
        return strlen(s) + 2;
    }
    return 0;
}

size_t ESPOutput::printMSGLine(const char *s)
{
    if (_webserver)
    {
        if (!_headerSent && !_footerSent)
        {
            _webserver->setContentLength(CONTENT_LENGTH_UNKNOWN);
            _webserver->sendHeader("Content-Type", "text/html");
            _webserver->sendHeader("Cache-Control", "no-cache");
            _webserver->send(_code);
            _headerSent = true;
        }
        if (_headerSent && !_footerSent)
        {
            _webserver->sendContent_P((const char *)s, strlen(s));
            _webserver->sendContent_P((const char *)"\n", 1);
            return strlen(s + 1);
        }
    }

    return 0;
}

size_t ESPOutput::printMSG(const char *s, bool withNL)
{

    if (_webserver)
    {
        if (!_headerSent && !_footerSent)
        {
            _webserver->sendHeader("Cache-Control", "no-cache");
#ifdef ESP_ACCESS_CONTROL_ALLOW_ORIGIN
            _webserver->sendHeader("Access-Control-Allow-Origin", "*");
#endif // ESP_ACCESS_CONTROL_ALLOw_ORIGIN
            _webserver->send(_code, "text/plain", s);
            _headerSent = true;
            _footerSent = true;
            return strlen(s);
        }
    }
    return 0;
}

size_t ESPOutput::printERROR(const char *s, int code_error)
{
    String display = "";

    (void)code_error;
    if (_webserver)
    {
        if (!_headerSent && !_footerSent)
        {
            _webserver->sendHeader("Cache-Control", "no-cache");
            if (s[0] != '{')
            {
                display = "error: ";
            }
            else
            {
                display = "";
            }
            display += s;
            _webserver->send(code_error, "text/plain", display.c_str());
            _headerSent = true;
            _footerSent = true;
            return display.length();
        }
    }
    return 0;
}

size_t ESPOutput::write(const uint8_t *buffer, size_t size)
{
    if (_webserver)
    {
        if (!_headerSent && !_footerSent)
        {
            _webserver->setContentLength(CONTENT_LENGTH_UNKNOWN);
            _webserver->sendHeader("Content-Type", "text/html");
            _webserver->sendHeader("Cache-Control", "no-cache");
            _webserver->send(_code);
            _headerSent = true;
        }
        if (_headerSent && !_footerSent)
        {
            _webserver->sendContent_P((const char *)buffer, size);
        }
    }
    return 0;
}
