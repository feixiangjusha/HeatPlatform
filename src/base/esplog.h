#ifndef _ESP_LOG_H
#define _ESP_LOG_H
#include "Arduino.h"
#include "espconfig.h"
#include "espwebsocket.h"
void initLog();

#if defined(DEBUG_OUTPUT_CHANNEL)
#if defined(ARDUINO_ARCH_ESP8266)
// no need with latest esp8266 core
#define pathToFileName(p) p
#endif // ARDUINO_ARCH_ESP8266

// Serial
#if (DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_SERIAL0) || (DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_SERIAL1) || (DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_SERIAL2)

#if DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_SERIAL0
#define DEBUG_OUTPUT_SERIAL Serial
#endif // DEBUG_OUTPUT_SERIAL0
#if DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_SERIAL1
#define DEBUG_OUTPUT_SERIAL Serial1
#endif // DEBUG_OUTPUT_SERIAL1
#ifdef ARDUINO_ARCH_ESP32
#if DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_SERIAL2
#define DEBUG_OUTPUT_SERIAL Serial2
#endif // DEBUG_OUTPUT_SERIAL2
#endif

#define log_error(format, ...) DEBUG_OUTPUT_SERIAL.printf("[ESP3D][%s:%u] %s(): " format "\r\n", pathToFileName(__FILE__), __LINE__, __FUNCTION__, ##__VA_ARGS__)

#endif // DEBUG_OUTPUT_SERIAL0 || DEBUG_OUTPUT_SERIAL1 || DEBUG_OUTPUT_SERIAL2
// websocket
#if DEBUG_OUTPUT_CHANNEL == DEBUG_OUTPUT_WEBSOCKET
#define log_error(format, ...) EspWebsocket::printf("{\"cmd\":\"log\",\"msg\":\"[%s:%u] %s(): " format "\"}", pathToFileName(__FILE__), __LINE__, __FUNCTION__, ##__VA_ARGS__)
#endif

#else
#define log_error(format, ...)
#endif // DEBUG_OUTPUT_CHANNEL
#if DEBUG_LEVEL <= DEBUG_LEVEL_INFO
#define log_info(format, ...) log_error(format, ##__VA_ARGS__)
#else
#define log_info(format, ...)
#endif // DEBUG_LEVEL==DEBUG_LEVEL_INFO
#if DEBUG_LEVEL <= DEBUG_LEVEL_DEBUG
#define log_debug(format, ...) log_info(format, ##__VA_ARGS__)
#else
#define log_debug(format, ...)
#endif // DEBUG_LEVEL==DEBUG_LEVEL_DEBUG
#endif //_ESP_LOG_H
