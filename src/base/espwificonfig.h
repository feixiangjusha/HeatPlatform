
#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#endif // ARDUINO_ARCH_ESP32
#ifdef ARDUINO_ARCH_ESP8266
#include <ESP8266WiFi.h>
#endif // ARDUINO_ARCH_ESP8266

#ifndef _ESP_WIFI_CONFIG_H
#define _ESP_WIFI_CONFIG_H

class WiFiConfig
{
public:
    static bool ConnectSTA2AP();
    static bool StartAP();
    static bool StartSTA();
    static bool begin(int8_t &espMode);
    static void end();
    static void handle();
};

#endif //_ESP_WIFI_CONFIG_H
