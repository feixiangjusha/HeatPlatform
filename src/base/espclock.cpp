#include "Arduino.h"
#include "espclock.h"
#include <Ticker.h>
uint64_t times = 0;
#ifndef INTERVAL_MILLIS
#define INTERVAL_MILLIS 5
#endif
Ticker timer; // 创建软件定时器对象
void timerCallback()
{
    times++;
}
void clockInit()
{
    timer.attach_ms(INTERVAL_MILLIS, timerCallback);
}

unsigned long getMillis()
{
    return times * INTERVAL_MILLIS;
}