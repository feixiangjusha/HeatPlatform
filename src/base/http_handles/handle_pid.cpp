/*
 handle-root.cpp - ESP3D http handle

 Copyright (c) 2014 Luc Lebosse. All rights reserved.

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with This code; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include "../espconfig.h"
#include "../httpserver.h"
// embedded response file if no files on ESP Filesystem
#include "embedded.h"
#include <ESPAsyncWebServer.h>
#include "ArduinoJson.h"
#include "../espuser.h"
#include "../espsettings.h"

// Root of Webserver/////////////////////////////////////////////////////
void HTTP_Server::handle_pid(AsyncWebServerRequest *request)
{
    struct PID pid = EspUser::refreshPID();
    // post请求获取参数异常，使用get请求设置pid

    // 设置PID
    String temp;
    bool isSet = false;
    if (request->hasParam("P"))
    {
        isSet = true;
        temp = request->getParam("P")->value();
        log_debug("set-P:%s", temp);
        Settings::writeString(KEY_PID_P, temp.c_str());
    }
    if (request->hasParam("I"))
    {
        isSet = true;
        temp = request->getParam("I")->value();
        log_debug("set-I:%s", temp);
        Settings::writeString(KEY_PID_I, temp.c_str());
    }
    if (request->hasParam("D"))
    {
        isSet = true;
        temp = request->getParam("D")->value();
        log_debug("set-D:%s", temp);
        Settings::writeString(KEY_PID_D, temp.c_str());
    }
    if (isSet)
        pid = EspUser::refreshPID();
    DynamicJsonDocument doc(200);
    doc["P"] = pid.Kp;
    doc["I"] = pid.Ki;
    doc["D"] = pid.Kd;
    String jsonString;
    serializeJson(doc, jsonString);
    log_info("pid:P-%f,I-%f,D-%f", pid.Kp, pid.Ki, pid.Kd);

    request->send(200, "application/json", jsonString.c_str());
}
