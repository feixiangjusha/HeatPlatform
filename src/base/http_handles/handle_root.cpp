/*
 handle-root.cpp - ESP3D http handle

 Copyright (c) 2014 Luc Lebosse. All rights reserved.

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with This code; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include "../espconfig.h"
#include "../httpserver.h"
// embedded response file if no files on ESP Filesystem
#include "embedded.h"
#include <ESPAsyncWebServer.h>
// Root of Webserver/////////////////////////////////////////////////////
void HTTP_Server::handle_root(AsyncWebServerRequest *request)
{
    // if no lets launch the default content
    AsyncWebServerResponse *response = request->beginResponse_P(200, "text/html", (const uint8_t *)tool_html_gz,tool_html_gz_size);
    response->addHeader("Content-Encoding", "gzip");
    request->send(response);
}
