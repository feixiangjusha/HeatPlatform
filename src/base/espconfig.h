#ifndef _ESP_CONFIG_H
#define _ESP_CONFIG_H
#include "Arduino.h"
#include "../configuration.h"
#include "esplog.h"

#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#endif // ARDUINO_ARCH_ESP32
#ifdef ARDUINO_ARCH_ESP8266
#include <ESP8266WiFi.h>
#endif // ARDUINO_ARCH_ESP8266


#endif