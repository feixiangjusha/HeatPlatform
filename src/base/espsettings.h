#ifndef _ESP_SETTINGS_H
#define _ESP_SETTINGS_H

#include "espconfig.h"

enum settingKey
{
    KEY_SETTINGS_VERSION, // 配置设定版本号
    KEY_RADIO_MODE,       // 网络模式，AP/STA...
    KEY_AP_SSID,
    KEY_AP_PASSWORD,
    KEY_STA_SSID,
    KEY_STA_PASSWORD,
    KEY_HEATING_TEMPERATURE,
    KEY_PID_P,
    KEY_PID_I,
    KEY_PID_D
};
struct defaultSetting
{
    int maxLen;
    const char *defaultValue;
    int pos;
};
class Settings
{
public:
    static bool begin();
    static bool reset();
    static defaultSetting getDefaultSetting(settingKey key);
    static uint8_t getDefaultSettingByte(settingKey key);
    static String getDefaultSettingString(settingKey key);
    static uint32_t getDefaultSettingUint32(settingKey key);
    static bool writeByte(settingKey key, const uint8_t value);
    static bool writeString(settingKey key, const char *byte_buffer);
    static bool writeUint32(settingKey key, const uint32_t value);
    static const char *readString(settingKey key, bool *haserror = NULL);
    static uint8_t readByte(settingKey key, bool *haserror = NULL);
    static uint32_t readUint32(settingKey key, bool *haserror = NULL);
    // static uint32_t readUint32(settingKey key, bool *haserror = NULL);
    // static uint32_t readIP(settingKey key, bool *haserror = NULL);
    static int8_t getSettingsVersion();

private:
    static void init(settingKey key, int maxLen, const char *defaultValue);
};
#endif