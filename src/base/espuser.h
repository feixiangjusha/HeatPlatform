
#ifndef _ESP_USER_H
#define _ESP_USER_H
#include "ArduinoJson.h"
struct PID
{
    double Kp;
    double Ki;
    double Kd;
};
class EspUser
{
public:
    static void begin();
    static void handle();
    static void setHeating(bool _heating);
    static void setTemp(double point);
    static DynamicJsonDocument getState();
    static PID refreshPID();
};
#endif //_EspUser_H
