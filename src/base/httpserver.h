
#ifndef _HTTP_SERVER_H
#define _HTTP_SERVER_H
#include "espconfig.h"
#include <ESPAsyncWebServer.h>
// class WebSocketsServer;
#if defined(ARDUINO_ARCH_ESP32)
#define WEBSERVER AsyncWebServer
#endif // ARDUINO_ARCH_ESP32
#if defined(ARDUINO_ARCH_ESP8266)
#define WEBSERVER AsyncWebServer
#endif // ARDUINO_ARCH_ESP8266

// Upload status
typedef enum
{
    UPLOAD_STATUS_NONE = 0,
    UPLOAD_STATUS_FAILED = 1,
    UPLOAD_STATUS_CANCELLED = 2,
    UPLOAD_STATUS_SUCCESSFUL = 3,
    UPLOAD_STATUS_ONGOING = 4
} upload_status_type;

class HTTP_Server
{
public:
    static bool begin();
    static void end();
    static void handle();
    static bool started()
    {
        return _started;
    }
    static uint16_t port()
    {
        return _port;
    }

private:
    static void pushError(int code, const char *st, uint16_t web_error = 500, uint16_t timeout = 1000);
    static bool _started;
    // static WEBSERVER *_webserver;
    static uint16_t _port;
    static uint8_t _upload_status;
    static void init_handlers();
    static void handle_root(AsyncWebServerRequest *request);
    static void handle_state(AsyncWebServerRequest *request);
    static void handle_pid(AsyncWebServerRequest *request);
};

#endif //_HTTP_SERVER_H
