#ifndef _ESP_WEBSOCKET_H
#define _ESP_WEBSOCKET_H
#include "ArduinoJson.h"
typedef void (*CallbackFunction)(DynamicJsonDocument);
class EspWebsocket
{
public:
    static void begin();
    static void handle();
    static void print(String str);
    static void print(DynamicJsonDocument json);
    static void attach(String type, CallbackFunction callback);
    static void printf(const char *format, ...);
};

#endif