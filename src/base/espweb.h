
#ifndef _ESPWEB_H
#define _ESPWEB_H
// be sure correct IDE and settings are used for ESP8266 or ESP32
#if !(defined(ARDUINO_ARCH_ESP8266) || defined(ARDUINO_ARCH_ESP32))
#error Oops!  Make sure you have 'ESP8266 or ESP32' compatible board selected from the 'Tools -> Boards' menu.
#endif // ARDUINO_ARCH_ESP8266 + ARDUINO_ARCH_ESP32
#include <Arduino.h>
class EspWeb
{
public:
    EspWeb();
    bool begin();
    void handle();
    static void restart_esp(bool need_restart = true);

private:
    static bool restart;
    void restart_now();
};
#endif //_ESPWEB_H
