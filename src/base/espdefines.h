#ifndef _ESP_DEFINES_H
#define _ESP_DEFINES_H
#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#endif // ARDUINO_ARCH_ESP32
#ifdef ARDUINO_ARCH_ESP8266
#include <ESP8266WiFi.h>
#endif // ARDUINO_ARCH_ESP8266
// 日志级别
#define DEBUG_LEVEL_DEBUG 0
#define DEBUG_LEVEL_INFO 1
#define DEBUG_LEVEL_ERROR 2

// 日志输出路径
#define DEBUG_OUTPUT_NO 0      // 不输出
#define DEBUG_OUTPUT_SERIAL0 1 // 串口0输出
#define DEBUG_OUTPUT_SERIAL1 2 // 串口1输出
#ifdef ARDUINO_ARCH_ESP32
#define DEBUG_OUTPUT_SERIAL2 3   // 串口2输出
#endif                           // ARDUINO_ARCH_ESP32
#define DEBUG_OUTPUT_WEBSOCKET 4 // websocket输出

// Settings
#define SETTINGS_IN_EEPROM 1
#define SETTINGS_IN_PREFERENCES 2

// WIFI模式
// #define ESP_NO_NETWORK 0 // 无网络
#define ESP_WIFI_STA 1 // STA模式
#define ESP_WIFI_AP 2  // AP模式

// File system
#define ESP_FILE_READ 0
#define ESP_FILE_WRITE 1
#define ESP_FILE_APPEND 2

#define FS_ROOT 0
#define FS_FLASH 1
#define FS_SD 2
#define FS_USBDISK 3
#define FS_UNKNOWN 254
#define MAX_FS 3


//Errors code
#define ESP_ERROR_AUTHENTICATION    1
#define ESP_ERROR_FILE_CREATION     2
#define ESP_ERROR_FILE_WRITE        3
#define ESP_ERROR_UPLOAD            4
#define ESP_ERROR_NOT_ENOUGH_SPACE  5
#define ESP_ERROR_UPLOAD_CANCELLED  6
#define ESP_ERROR_FILE_CLOSE        7
#define ESP_ERROR_NO_SD             8
#define ESP_ERROR_MOUNT_SD          9
#define ESP_ERROR_RESET_NUMBERING   10
#define ESP_ERROR_BUFFER_OVERFLOW   11
#define ESP_ERROR_START_UPLOAD      12
#define ESP_ERROR_SIZE              13
#define ESP_ERROR_UPDATE            14
#endif